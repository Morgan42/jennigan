<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class candidacys extends Model
{
    protected $table = "candidacys";
    protected $fillable = ["user_id", "job_id"];
    protected $dates = ["created_at", "update_at", "deleted_at"];
}