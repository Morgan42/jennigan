<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clients extends Model
{
    protected $table = "clients";

    protected $fillable = [
        "files", "name_society", "name_contact", "email_contact", "phone", "activity", "note"
    ];
    
    protected $dates = [
        "created_at", "updated_at", "deleted_at"
    ];
}
