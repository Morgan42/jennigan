<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobs extends Model
{
    protected $table = "jobs";

    protected $fillable = [
        "title", "type", "description", "location", "category", "salary", "enable", "note", "closing_at"
    ];
    
    protected $dates = [
        "created_at", "updated_at"
    ];
}
