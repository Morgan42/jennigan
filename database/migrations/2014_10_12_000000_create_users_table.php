<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lastname', 45);
            $table->string('fristname', 45);
            $table->string('adress', 250);
            $table->string('country', 45);
            $table->string('nationalty', 45);
            $table->string('birthdate', 45);
            $table->string('birthplace', 45);
            $table->string('gender', 45);
            $table->string('currentlocation', 250);
            $table->string('passeport', 255);
            $table->string('cv', 255);
            $table->string('job_sector', 45);            
            $table->string('experience', 250);
            $table->string('short_description', 250);
            $table->text('note');
            $table->string('availibity', 45);
            $table->string('email');
            $table->string('password', 45);
            $table->tinyInteger('admin');            
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
