<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\CandidacysController;
use Illuminate\Support\Facades\Auth;


class UsersController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function index()
    {
        $userDatas = $this->user->where('id', Auth::id())->get();

        return view('auth/profile', compact('userDatas'));
    }


    public function store(Request $request)
    {
        $requestData = $request->all();
  
        $this->user->where('id', Auth::id())->update(array(
        'lastname' => $requestData['lastname'],
        'fristname' => $requestData['firstname'],
        'adress' => $requestData['adress'],
        'country' => $requestData['country'],
        'nationalty' => $requestData['nationalty'],
        'birthdate' => $requestData['birthdate'],
        'birthplace' => $requestData['birthplace'],
        'gender' => $requestData['gender'],
        'currentlocation' => $requestData['currentlocation'],
        'job_sector' => $requestData['job_sector'],
        'experience' => $requestData['experience'],
        'short_description' => $requestData['short_description'],
        ));

        return back();
    }
}