<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";
    
    protected $fillable = [
        'lastname', 'firstname', 'adress', 'country', 'nationalty', 'birthdate',
        'birthplace', 'gender', 'currentlocation', 'passeport', 'cv', 'job_sector', 'experience',
        'short_description', 'note', 'availibity', 'email', 'password', 'admin', 'deleted_at'
    ];

    protected $dates = [
        "created_at", "update_at"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
