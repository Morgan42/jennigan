<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\clients;
use App\jobs;

class AdminController extends Controller
{
    protected $jobs;
    protected $clients;

    public function __construct(jobs $jobs, clients $clients)
    {
        $this->jobs = $jobs;
        $this->clients = $clients;
    }

    public function index()
    {
        $adminJob = $this->jobs->all();
        $adminClient = $this->clients->all();

        return view ('admin', compact('adminJob', 'adminClient'));
    }

    public function destroy($id)
      {
        $clients = clients::find($id);
        $clients->delete();

        return redirect('admin')->with('success', 'Stock has been deleted Successfully');
      }
}
