<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\jobs;
use App\Http\Controllers\ClientsController;

class jobsController extends Controller
{
    protected $jobs;

    public function __construct(jobs $jobs)
    {
        $this->jobs = $jobs;
    }

    public function store(Request $request)
    {
        $requestData = $request->all();


        $this->jobs->create(array(
        'title' => $requestData['title'],
        'type' => $requestData['type'],
        'description' => $requestData['description'],
        'location' => $requestData['location'],
        'category' => $requestData['category'],
        'salary' => $requestData['salary'],
        'closing_at' => $requestData['closing_at'],
        ));

        return back();
    }

    public function index()
    {
        $toto = $this->jobs->all();

        return view ('index', compact('toto'));
    }
    
    public function jobOffer()
    {
        $toto = $this->jobs->all();

        return view ('jobs/index', compact('toto'));
    }

    public function show($id)
    {
        $job = jobs::find($id);

        return view('jobs/show', compact('job'));
    }
}
