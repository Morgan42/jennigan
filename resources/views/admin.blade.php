@extends('layouts.base')
@section('title', 'Profil')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <section class="gray-bg">
        <div class="container">
        </div>
    </section>
        <section class="gray-bg">
            <div class="container">
                <div class="row">
                    <div class="score-container">
                        <br><br>
                        <h3>
                            Admin Space
                        </h3>
                    </div>
                </div>
            </div>
            <br><hr>
        </section>
            <section>
                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s6"><a href="#test2">Création d'un client</a></li>
                            <li class="tab col s6"><a class="active"  href="#createJobs">Création d'une offre d'emploi</a></li>
                            <li class="tab col s6"><a class="active"  href="#test3">Tableau des clients et offres</a></li>
                        </ul>
                    </div>
                    <div id="test2" class="col s12">
                    
                            <form method="post" action="admin/clients" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data"><input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
                            {{ csrf_field() }}
                            <section class="section-padding">
                                    <div class="container">
                                        <div class="row">
                                            <h3 class="text-extrabold">Création d'un client</h3>
                                            <div class="clearfix visible-sm"></div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="name_society" id="name_society" value="" required>
                                                    <label for="name_society">Society Name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="name_contact" id="name_contact" value="" required>
                                                    <label for="name_contact">Contact name</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input type="text" class="form-control" name="email_contact" id="email_contact" value="" required>
                                                    <label for="email_contact">Contact email</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <input required="required" id="activity" name="activity" type="text" value="">
                                                    <label for="activity">Activity</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-4">
                                                <div class="input-field">
                                                    <label for="phone">Phone</label>
                                                    <br>
                                                    <input required="required" id="phone" name="phone" type="phone" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    </section>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                                    <span><strong>Creer</strong></span>
                                </button>
                            </div>
                        </form>
                    <section>    
                    </div>
                    <div id="createJobs" class="col s12">
                        <form method="post" action="admin/jobs" accept-charset="UTF-8" id="candidateForm" role="form" data-parsley-validate="" enctype="multipart/form-data"><input name="_token" type="hidden" value="UYfchy67WuTVzhSstPK1RDZEIfgqpKCcLnLyhf8a">
                        {{ csrf_field() }} 
                        <section class="section-padding">
                                <div class="container">
                                    <div class="row">
                                        <h3 class="text-extrabold">Création d'une offre d'emploi</h3>
                                        <div class="clearfix visible-sm"></div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="title" id="title" value="" required>
                                                <label for="title">Title</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="text" class="form-control" name="salary" id="salary" value="" required>
                                                <label for="salary">Salary</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <div class="input-field">
                                                <input type="date" class="form-control" name="closing_at" id="closing_at" value="" required>
                                                <label for="closing_at"></label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="input-field">
                                                <textarea class="materialize-textarea" id="description" name="description" cols="50" rows="10"></textarea>
                                                <label for="description">Short description for your offers </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="category">Category</label>
                                                <br>
                                                <select id="category" type="select" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category" value="{{ old('category') }}" required autofocus>
                                                    <option selected value="Commercial">Commercial</option>
                                                    <option value="RetailSales">Retail sales</option>
                                                    <option value="Creative">Creative</option>
                                                    <option value="Technology">Technology</option>
                                                    <option value="Marketing & PR">Marketing & PR</option>
                                                    <option value="Fashion & Luxury">Fashion & luxury</option>
                                                    <option value="Management & HR">Management & HR</option>
                                                </select>
                                                <span class="help-block">category</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-field">
                                                <label for="type">Type</label>
                                                <br>
                                                <select id="type" type="select" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type" value="{{ old('type') }}" required autofocus>
                                                    <option selected value="Fulltime">Fulltime</option>
                                                    <option value="Parttime">Parttime</option>
                                                    <option value="Temporary">Temporary</option>
                                                    <option value="Freelance">Freelance</option>
                                                    <option value="Seasonal">Seasonal</option>
                                                </select>
                                                <span class="help-block">type</span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="input-field">
                                                <input id="location" name="location" type="text" value="">
                                                <label for="location">location</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-block btn-lg gradient secondary waves-effect waves-light">
                                    <span><strong>CREATE</strong></span>
                                </button>
                            </div>
                        </form>
                        </div>
                    </div>
                </section>

                <div class="container">
                    <div id="test3" class="col s12">
                        <h4>
                            <center>Clients</center>
                        </h4>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th scope="col">Id client</th>
                                <th scope="col">Name</th>
                                <th scope="col">Contact</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Activity</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($adminClient as $client)
                                <tr>
                                    <th scope="row">{{ $client->id }}</th>
                                        <td>{{ $client->name_society }}</td>
                                        <td>{{ $client->name_contact }}</td>
                                        <td>{{ $client->email_contact }}</td>
                                        <td>{{ $client->phone }}</td>
                                        <td>{{ $client->activity }}</td>
                                </tr>
                            @empty
                                <p>No Clients</p>
                            @endforelse
                            </tbody>
                        </table>
                    
                        <h4>
                            <center>Offres</center>
                        </h4>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                <th scope="col">Id offre</th>
                                <th scope="col">title</th>
                                <th scope="col">type</th>
                                <th scope="col">location</th>
                                <th scope="col">category</th>
                                <th scope="col">salary</th>
                                <th scope="col">start date</th>
                                <th scope="col">Id client</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($adminJob as $job)
                                <tr>
                                    <th scope="row">{{ $job->id }}</th>
                                        <td>{{ $job->title }}</td>
                                        <td>{{ $job->type }}</td>
                                        <td>{{ $job->location }}</td>
                                        <td>{{ $job->category }}</td>
                                        <td>{{ $job->salary }}&euro;</td>
                                        <td>{{ $job->closing_at }}</td>
                                        <td>{{ $client->id }}</td>
                                </tr>
                            @empty
                                <p>No Jobs</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
                
                
@endsection
