<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('admin/clients', 'clientsController@store');
Route::post('admin/jobs', 'jobsController@store');
Route::get('/', 'jobsController@index');
Route::get('/index', 'jobsController@index')->name('index');
Route::get('jobs_index', 'jobsController@jobOffer')->name('jobs_index');
Route::get('jobs_show/{id}', 'jobsController@show')->name('jobs_show');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::get('/admin', 'clientsController@indexClient')->name('admin');
Route::post('auth/profile/store', 'UsersController@store')->name('storeProfile');
Route::get('auth/profile', 'UsersController@index')->name('profile');
Route::post('/jobs/apply', 'CandidacysController@store')->name('apply');
Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/company', function () {
    return view('company');
})->name('company');

Route::get('auth/login', function () {
    return view('auth/login');
})->name('auth_login');

Route::get('auth/register', function () {
    return view('auth/register');
})->name('auth_register');



