<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\clients;
use App\Http\Controllers\ClientsController;
use DB;

class clientsController extends Controller
{

    protected $client;

    public function __construct(clients $client)
    {
        $this->client = $client;
    }

    public function store(Request $request)
    {
        $requestData = $request->all();


        $this->client->create(array(
        'name_society' => $requestData['name_society'],
        'name_contact' => $requestData['name_contact'],
        'email_contact' => $requestData['email_contact'],
        'phone' => $requestData['phone'],
        'activity' => $requestData['activity'],
        ));

        return back();
    }

    public function indexClient()
    {
        $hein = $this->client->all();

        return view ('admin', compact('hein'));
    }
}