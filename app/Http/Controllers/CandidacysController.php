<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\CandidacysController;
use App\candidacys;
use App\Http\Controllers\UsersController;
use App\jobs;
use App\Http\Controllers\JobsController;
use Illuminate\Support\Facades\Auth;


class CandidacysController extends Controller
{
    protected $candidacy;

    public function __construct(candidacys $candidacy)
    {
        $this->candidacy = $candidacy;
    }

    // public function index()
    // {
    //    $apply = $this->candidacys->all();
    //    return back();
    // }

    public function store(Request $request)
    {
        $requestData = $request->all();
        $this->candidacy->create(array(
        'job_id' => $requestData['button'],
        'user_id' => Auth::id()
        ));

        return back();
    }
}
